#ifndef _POSTGRES_DB_H
#define _POSTGRES_DB_H
#include <libpq/libpq-fs.h>
#include <libpq-fe.h>
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;


class AggregateAttrib
{
public:
	string field;
	int group_var;
	string aggregate_func;
	
	AggregateAttrib(string f, int g_v, string agg_func):field(f),group_var(g_v),aggregate_func(agg_func)
	{
	}

	string getName()
	{
		char n[100]; 
		itoa(group_var,n,10);
		return aggregate_func+"_"+string(n)+"_"+field;
	}
};


class Sum
{
public:
	Sum(string f, int g_v, string agg_func):attrib(f,g_v,agg_func),value(0)
	{
	}
	void update(float val)
	{
		value += val;
	}
	AggregateAttrib attrib;
	float value;
};

class Max
{
public:
	Max(string f, int g_v, string agg_func):attrib(f,g_v,agg_func),value(0)
	{
	}
	void update(float val)
	{
		if(val>=value)
		{
			value = val;
		}
	}
	AggregateAttrib attrib;
	float value;
};

class Min
{
public:
	Min(string f, int g_v, string agg_func):attrib(f,g_v,agg_func),value(0)
	{
	}
	void update(float val)
	{
		if(val<=value)
		{
			value = val;
		}
	}
	AggregateAttrib attrib;
	float value;
};

class Avg
{
public:
	Avg(string f, int g_v, string agg_func):attrib(f,g_v,agg_func),value(0),sum(0),count(0)
	{
	}
	void update(float val)
	{
		sum += val;
		count++;
		if(count == 0)
		{
			value = 0;
		}else{
			value = sum/float(count);
		}
	}
	AggregateAttrib attrib;
	float value;
	float sum;
	int count;
};

class Count
{
public:
	Count(string f, int g_v, string agg_func):attrib(f,g_v,agg_func),value(0)
	{
	}
	void update(float val)
	{
		value++;
	}
	void update(string val)
	{
		value++;
	}
	AggregateAttrib attrib;
	int value;
};


class PostgresDB
{
public:
	struct result
	{
		PGresult* rec;
		int rows;
		int columns;
		vector<string> fields;
		vector<Oid> types;

		result()
		{
			rows = 0;
			columns = 0;
			rec = NULL;
		}
		
		string getValue(int row, string field)
		{
			if(rec)
			{
				return PQgetvalue(rec,row,PQfnumber(rec,field.c_str()));
			}
			return "None";
		}

		string getValue(int row, int column)
		{
			if(rec)
			{
				return PQgetvalue(rec,row,column);
			}
			return "None";
		}

		Oid getType(string field)
		{
			if(rec)
			{
				return PQftype(rec, PQfnumber(rec,field.c_str()));
			}
			return 0;
		}
	};

	PostgresDB();

	~PostgresDB();

	bool connectdb(string dbname, string username, string pwd);

	PGresult* exec_command(const char * command, ExecStatusType checkStatus);

	bool start_transaction();

	bool end_transaction();

	bool init_result(PGresult* query_rec, result& rec_struct);

	PGconn* conn;

	ConnStatusType conn_rec;
};

#endif //_POSTGRES_DB_H