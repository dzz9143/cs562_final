#include <libpq-fe.h>
#include <iostream>
#include "postgresdb.h"
#include "ProcessEngine.h"
#define and &&
#define or ||
using namespace std;
struct mf_structure{
	string prod;
	int month;
	Avg* Avg_1_quant;
	Avg* Avg_2_quant;
	Count* Count_3_quant;
};
vector<mf_structure> list;
struct row{
	string cust;
	string prod;
	int day;
	int month;
	int year;
	string state;
	int quant;
};
void Check(vector<mf_structure>& list, row& r,int g_v){
	if(g_v ==0){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod&&list[i].month==r.month){
				if(list[i].Avg_1_quant->attrib.group_var == g_v){
					list[i].Avg_1_quant->update(r.quant);
				}
				if(list[i].Avg_2_quant->attrib.group_var == g_v){
					list[i].Avg_2_quant->update(r.quant);
				}
				if(list[i].Count_3_quant->attrib.group_var == g_v){
					list[i].Count_3_quant->update(r.quant);
				}
				return;
			}
		}
		mf_structure new_struct;
		new_struct.prod = r.prod;
		new_struct.month = r.month;
		new_struct.Avg_1_quant = new Avg("quant",1,"Avg");
		new_struct.Avg_2_quant = new Avg("quant",2,"Avg");
		new_struct.Count_3_quant = new Count("quant",3,"Count");
		list.push_back(new_struct);
	}
	if(g_v ==1){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod&&list[i].month==r.month-1){
				if(list[i].Avg_1_quant->attrib.group_var == g_v){
					list[i].Avg_1_quant->update(r.quant);
				}
				if(list[i].Avg_2_quant->attrib.group_var == g_v){
					list[i].Avg_2_quant->update(r.quant);
				}
				if(list[i].Count_3_quant->attrib.group_var == g_v){
					list[i].Count_3_quant->update(r.quant);
				}
			}
		}
	}
	if(g_v ==2){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod&&list[i].month==r.month+1){
				if(list[i].Avg_1_quant->attrib.group_var == g_v){
					list[i].Avg_1_quant->update(r.quant);
				}
				if(list[i].Avg_2_quant->attrib.group_var == g_v){
					list[i].Avg_2_quant->update(r.quant);
				}
				if(list[i].Count_3_quant->attrib.group_var == g_v){
					list[i].Count_3_quant->update(r.quant);
				}
			}
		}
	}
	if(g_v ==3){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod&&list[i].month==r.month&&r.quant>list[i].Avg_1_quant->value&&r.quant<list[i].Avg_2_quant->value){
				if(list[i].Avg_1_quant->attrib.group_var == g_v){
					list[i].Avg_1_quant->update(r.quant);
				}
				if(list[i].Avg_2_quant->attrib.group_var == g_v){
					list[i].Avg_2_quant->update(r.quant);
				}
				if(list[i].Count_3_quant->attrib.group_var == g_v){
					list[i].Count_3_quant->update(r.quant);
				}
			}
		}
	}
}
int main(){
	PostgresDB pbd;pbd.connectdb("cs562","postgres","zhizhuoding9143");
	PGresult* querry_rec = NULL;
	PGresult* tuple_rec = NULL;
	pbd.start_transaction();
	if((querry_rec = pbd.exec_command("DECLARE myportal CURSOR FOR select * from sales",PGRES_COMMAND_OK)) != NULL){
		PQclear(querry_rec);
		tuple_rec = pbd.exec_command("FETCH ALL in myportal", PGRES_TUPLES_OK);
	}
	struct PostgresDB::result rec_struct;
	if(tuple_rec != NULL){
		pbd.init_result(tuple_rec,rec_struct);
		cout<<rec_struct.rows<<endl;
		cout<<rec_struct.columns<<endl;
	}else{
		cout<<"No data"<<endl;
	}
	for(int gv = 0; gv<4; ++gv){
		for(int i = 0; i<rec_struct.rows; ++i){
			row new_row;
			new_row.cust = rec_struct.getValue(i,"cust");
			new_row.prod = rec_struct.getValue(i,"prod");
			new_row.day = atoi(rec_struct.getValue(i,"day").c_str());
			new_row.month = atoi(rec_struct.getValue(i,"month").c_str());
			new_row.year = atoi(rec_struct.getValue(i,"year").c_str());
			new_row.state = rec_struct.getValue(i,"state");
			new_row.quant = atoi(rec_struct.getValue(i,"quant").c_str());
			Check(list, new_row,gv);
		}
	}
	for(int i = 0; i<list.size(); ++i){
		cout<<list[i].prod<<" "<<list[i].month<<" "<<list[i].Count_3_quant->value<<" "<<endl;
	}
	cout<<list.size()<<endl;
	PQclear(tuple_rec);
	pbd.end_transaction();
	return 0;
}
