#ifndef _PROCESS_ENGINE_H
#define _PROCESS_ENGINE_H
#include <libpq-fe.h>
#include <vector>
#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <fstream>
#include <sstream>

#include "postgresdb.h"
#define FOR(i,size) for(int i = 0; i != size; ++i)

using namespace std;


class ProcessEngine
{
public:
	ProcessEngine();
	~ProcessEngine();
	string GetType(Oid id);
	void ReadQuery(const char * fileName);
	void ReadQuery2(const char * fileName);
	void GenMFStruct(ofstream& output);
	void GenCheckFunc(ofstream& output);
	void GenMainFunc(ofstream& output);
	void GenIncludes(ofstream& output);
	void GenOutput(ofstream& output);

	vector<string>& GetGroupAttribs();
	vector<AggregateAttrib>& GetAggregateFuncs();
	vector<string>& GetConditions();
	vector<string>& GetProjAttribs();

	//data member
	int num_gourp_var;
	vector<string> proj_attrib;
	vector<string> group_attrib;
	vector<AggregateAttrib> aggregate_func;
	vector<string> conditions;
	vector<string> row_fields;
	vector<string> row_field_types;
	string having_predicate;
};


#endif //_PROCESS_ENGINE_H