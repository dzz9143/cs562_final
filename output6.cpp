#include <libpq-fe.h>
#include <iostream>
#include "postgresdb.h"
#include "ProcessEngine.h"
#define and &&
#define or ||
using namespace std;
struct mf_structure{
	string prod;
	int quant;
	Count* Count_1_prod;
	Count* Count_2_prod;
};
vector<mf_structure> list;
struct row{
	string cust;
	string prod;
	int day;
	int month;
	int year;
	string state;
	int quant;
};
void Check(vector<mf_structure>& list, row& r,int g_v){
	if(g_v ==0){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod&&list[i].quant==r.quant){
				if(list[i].Count_1_prod->attrib.group_var == g_v){
					list[i].Count_1_prod->update(r.prod);
				}
				if(list[i].Count_2_prod->attrib.group_var == g_v){
					list[i].Count_2_prod->update(r.prod);
				}
				return;
			}
		}
		mf_structure new_struct;
		new_struct.prod = r.prod;
		new_struct.quant = r.quant;
		new_struct.Count_1_prod = new Count("prod",1,"Count");
		new_struct.Count_2_prod = new Count("prod",2,"Count");
		list.push_back(new_struct);
	}
	if(g_v ==1){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod){
				if(list[i].Count_1_prod->attrib.group_var == g_v){
					list[i].Count_1_prod->update(r.prod);
				}
				if(list[i].Count_2_prod->attrib.group_var == g_v){
					list[i].Count_2_prod->update(r.prod);
				}
			}
		}
	}
	if(g_v ==2){
		for(int i = 0;i < list.size() ; ++i){
			if (list[i].prod==r.prod&&r.quant<list[i].quant){
				if(list[i].Count_1_prod->attrib.group_var == g_v){
					list[i].Count_1_prod->update(r.prod);
				}
				if(list[i].Count_2_prod->attrib.group_var == g_v){
					list[i].Count_2_prod->update(r.prod);
				}
			}
		}
	}
}
int main(){
	PostgresDB pbd;pbd.connectdb("cs562","postgres","zhizhuoding9143");
	PGresult* querry_rec = NULL;
	PGresult* tuple_rec = NULL;
	pbd.start_transaction();
	if((querry_rec = pbd.exec_command("DECLARE myportal CURSOR FOR select * from sales",PGRES_COMMAND_OK)) != NULL){
		PQclear(querry_rec);
		tuple_rec = pbd.exec_command("FETCH ALL in myportal", PGRES_TUPLES_OK);
	}
	struct PostgresDB::result rec_struct;
	if(tuple_rec != NULL){
		pbd.init_result(tuple_rec,rec_struct);
		cout<<rec_struct.rows<<endl;
		cout<<rec_struct.columns<<endl;
	}else{
		cout<<"No data"<<endl;
	}
	for(int gv = 0; gv<3; ++gv){
		for(int i = 0; i<rec_struct.rows; ++i){
			row new_row;
			new_row.cust = rec_struct.getValue(i,"cust");
			new_row.prod = rec_struct.getValue(i,"prod");
			new_row.day = atoi(rec_struct.getValue(i,"day").c_str());
			new_row.month = atoi(rec_struct.getValue(i,"month").c_str());
			new_row.year = atoi(rec_struct.getValue(i,"year").c_str());
			new_row.state = rec_struct.getValue(i,"state");
			new_row.quant = atoi(rec_struct.getValue(i,"quant").c_str());
			Check(list, new_row,gv);
		}
	}
	for(int i = 0; i<list.size(); ++i){
		if(list[i].Count_2_prod->value==list[i].Count_1_prod->value/2){
			cout<<list[i].prod<<" "<<list[i].quant<<" "<<endl;
		}
	}
	cout<<list.size()<<endl;
	PQclear(tuple_rec);
	pbd.end_transaction();
	return 0;
}
