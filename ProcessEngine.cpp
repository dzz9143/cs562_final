#include "ProcessEngine.h"

//For ProcessEngine Class
ProcessEngine::ProcessEngine()
{

}

ProcessEngine::~ProcessEngine()
{

}

vector<string>& ProcessEngine::GetGroupAttribs()
{
	return group_attrib;
}

vector<AggregateAttrib>& ProcessEngine::GetAggregateFuncs()
{
	return aggregate_func;
}

vector<string>& ProcessEngine::GetConditions()
{
	return conditions;
}


vector<string>& ProcessEngine::GetProjAttribs()
{
	return proj_attrib;
}

string ProcessEngine::GetType(Oid id)
{
	switch(id)
	{
	case 1042:
	case 1043:
		return "string";
		break;
	case 23:
		return "int";
		break;
	default:
		return "None";
		break;
	}
}

//Mf-struct is based on the grouping attribute
//And Aggregate Functions
void ProcessEngine::GenMFStruct(ofstream& fileObj)
{
	PostgresDB pbd;
	
	pbd.connectdb("cs562","postgres","zhizhuoding9143");
	
	//Command Result
	PGresult* command_rec = NULL;
	//The Result from SQL
	PGresult* tuple_rec = NULL;
	
	pbd.start_transaction();

	if((command_rec = pbd.exec_command("DECLARE myportal CURSOR FOR select * from sales",PGRES_COMMAND_OK)) != NULL)
	{
		PQclear(command_rec);
		tuple_rec = pbd.exec_command("FETCH ALL in myportal", PGRES_TUPLES_OK);
	}

	fileObj<<"struct mf_structure{\n";

	//The structure that contains the fields  name and type.
	struct PostgresDB::result rec_struct;

	if(tuple_rec != NULL)
	{
		pbd.init_result(tuple_rec,rec_struct);
		for(int i = 0; i != GetGroupAttribs().size(); ++i)
		{
			fileObj<<"\t";
			fileObj<<GetType(rec_struct.getType(GetGroupAttribs()[i]))<<" "<<GetGroupAttribs()[i]<<";\n";
		}

		for(int i = 0; i != GetAggregateFuncs().size(); ++i)
		{
			fileObj<<"\t";
			//fileObj<<GetType(rec_struct.getType(GetAggregateFuncs()[i]->attrib.field))<<" "<<GetAggregateFuncs()[i]->attrib.getName()<<";\n";
			fileObj<<GetAggregateFuncs()[i].aggregate_func<<"* "<<GetAggregateFuncs()[i].getName()<<";\n";
		}

	}else{
		cout<<"No Schema Data!"<<endl;
		return;
	}

	fileObj<<"};\n";
	fileObj<<"vector<mf_structure> list;\n";

	//--------------------The Structure to Receive one Row-----------------------
	fileObj<<"struct row{\n";
	for(int i = 0; i != rec_struct.columns; ++i)
	{
		fileObj<<"\t";
		fileObj<<GetType(rec_struct.types[i]);
		row_field_types.push_back(GetType(rec_struct.types[i]));
		fileObj<<" ";
		fileObj<<rec_struct.fields[i]<<";\n";
		row_fields.push_back(rec_struct.fields[i]);
	}
	fileObj<<"};\n";


	PQclear(tuple_rec);
	pbd.end_transaction();
}

void ProcessEngine::GenCheckFunc(ofstream& output)
{
	output<<"void Check(vector<mf_structure>& list, row& r,int g_v){\n";

	FOR(gv, num_gourp_var+1)
	{
		output<<"\tif(g_v =="<<gv<<"){\n";
		output<<"\t\tfor(int i = 0;i < list.size() ; ++i){\n";
		if(gv == 0)
		{
			output<<"\t\t\tif ("<<conditions[0]<<"){\n";
			//Update List here.
			FOR(i,aggregate_func.size())
			{
				output<<"\t\t\t\tif(list[i]."<<aggregate_func[i].getName()<<"->"<<"attrib.group_var == "<<"g_v){"<<"\n";
				output<<"\t\t\t\t\tlist[i]."<<aggregate_func[i].getName()<<"->update(r."<<aggregate_func[i].field<<");\n";
				output<<"\t\t\t\t}\n";
			}

			output<<"\t\t\t\treturn;\n";
			output<<"\t\t\t}\n";
			output<<"\t\t}\n";
			//create new and add
			output<<"\t\tmf_structure new_struct;\n";
			FOR(i,group_attrib.size())
			{
				output<<"\t\tnew_struct."<<group_attrib[i]<<" = "<<"r."<< group_attrib[i]<<";\n";
			}

			FOR(i,aggregate_func.size())
			{
				output<<"\t\tnew_struct."<<aggregate_func[i].getName()<<" = new "<<aggregate_func[i].aggregate_func<<"("<<'"'<<aggregate_func[i].field<<'"'<<","<<aggregate_func[i].group_var<<","<<'"'<<aggregate_func[i].aggregate_func<<'"'<<")"<<";\n";
			}

			output<<"\t\tlist.push_back(new_struct);\n";
		
		}else{
			output<<"\t\t\tif ("<<conditions[gv]<<"){\n";
			//Update List here.
			FOR(i,aggregate_func.size())
			{
				output<<"\t\t\t\tif(list[i]."<<aggregate_func[i].getName()<<"->"<<"attrib.group_var == "<<"g_v){"<<"\n";
				output<<"\t\t\t\t\tlist[i]."<<aggregate_func[i].getName()<<"->update(r."<<aggregate_func[i].field<<");\n";
				output<<"\t\t\t\t}\n";
			}
			output<<"\t\t\t}\n";
			output<<"\t\t}\n";
		}
		output<<"\t}\n";
	}
	output<<"}\n";
}

void ProcessEngine::GenMainFunc(ofstream& output)
{
	output<<"int main(){\n";
	//connect to db
	output<<"\tPostgresDB pbd;pbd.connectdb("<<'"'<<"cs562"<<'"'<<","<<'"'<<"postgres"<<'"'<<","<<'"'<<"zhizhuoding9143"<<'"'<<");\n";
	output<<"	PGresult* querry_rec = NULL;\n\tPGresult* tuple_rec = NULL;\n";
	output<<"\tpbd.start_transaction();\n";
	//select * from sales
	output<<"\tif((querry_rec = pbd.exec_command("<<'"'<<"DECLARE myportal CURSOR FOR select * from sales"<<'"'<<",PGRES_COMMAND_OK)) != NULL){\n";
	output<<"\t\tPQclear(querry_rec);\n\t\ttuple_rec = pbd.exec_command("<<'"'<<"FETCH ALL in myportal"<<'"'<<", PGRES_TUPLES_OK);\n\t}\n";
	output<<"	struct PostgresDB::result rec_struct;\n";
	output<<"\tif(tuple_rec != NULL){\n\t\tpbd.init_result(tuple_rec,rec_struct);\n\t\tcout<<rec_struct.rows<<endl;\n\t\tcout<<rec_struct.columns<<endl;\n\t}else{\n\t\tcout<<"<<'"'<<"No data"<<'"'<<"<<endl;\n\t}\n";

	//scan the tuples
	output<<"\tfor(int gv = 0; gv<"<<num_gourp_var+1<<"; ++gv){\n";
		output<<"\t\tfor(int i = 0; i<rec_struct.rows; ++i){\n";
		output<<"\t\t\trow new_row;\n";
		FOR(i, row_fields.size())
		{
			if(row_field_types[i] == "int")
			{
				output<<"\t\t\t";
				output<<"new_row."<<row_fields[i]<<" = "<<"atoi(rec_struct.getValue(i,"<<'"'<<row_fields[i]<<'"'<<").c_str());\n";
			}
			if(row_field_types[i] == "string")
			{
				output<<"\t\t\t";
				output<<"new_row."<<row_fields[i]<<" = "<<"rec_struct.getValue(i,"<<'"'<<row_fields[i]<<'"'<<");\n";
			}
		}
		output<<"\t\t\t";
		output<<"Check(list, new_row,gv);\n";
		output<<"\t\t}\n";
	output<<"\t}\n";


	GenOutput(output);
	
	output<<"\tcout<<list.size()<<endl;\n";
	output<<"\tPQclear(tuple_rec);\n";
	output<<"\tpbd.end_transaction();\n";
	
	output<<"\treturn 0;\n";
	output<<"}\n";
}

void ProcessEngine::GenIncludes(ofstream& output)
{

	output<<"#include <libpq-fe.h>\n";
	output<<"#include <iostream>\n";
	output<<"#include "<<'"'<<"postgresdb.h"<<'"'<<"\n";
	output<<"#include "<<'"'<<"ProcessEngine.h"<<'"'<<"\n";
	output<<"#define and &&\n";
	output<<"#define or ||\n";
	output<<"using namespace std;\n";
}

void ProcessEngine::GenOutput(ofstream& output)
{
	output<<"\tfor(int i = 0; i<list.size(); ++i){\n";
	if(having_predicate != "null")
	{
		output<<"\t\tif("<<having_predicate<<"){\n";
			output<<"\t\t\tcout";
			FOR(i, proj_attrib.size())
			{
				output<<"<<"<<proj_attrib[i]<<"<<"<<'"'<<" "<<'"';
			}
			output<<"<<endl;\n";
		output<<"\t\t}\n";
	}else{
		output<<"\t\tcout";
		FOR(i, proj_attrib.size())
		{
			output<<"<<"<<proj_attrib[i]<<"<<"<<'"'<<" "<<'"';
		}
		output<<"<<endl;\n";
	}
	output<<"\t}\n";
	
}


void ProcessEngine::ReadQuery(const char * fileName)
{
	std::fstream file(fileName,ofstream::in);
	string line;
	string attrib;
	//read the proj attrib
	getline(file,line);//get the first line
	istringstream stream(line);
	while(stream>>attrib)
	{
		proj_attrib.push_back(attrib);
	}
	stream.clear();
	//---------------------------

	//read the grouping attrib
	getline(file,line);
	stream.str(line);
	while(stream>>attrib)
	{
		group_attrib.push_back(attrib);
	}
	stream.clear();

	//read the grouping var number
	getline(file,line);
	num_gourp_var  = atoi(line.c_str());

	//read the aggregate
	getline(file,line);
	stream.str(line);
	while(stream>>attrib)
	{
		int gv_num;
		string gv;
		string func;
		stream>>gv;
		stream>>func;
		gv_num = atoi(gv.c_str());
		aggregate_func.push_back(AggregateAttrib(attrib,gv_num,func));
	}
	stream.clear();
	
	//read the conditions
	getline(file,line);
	stream.str(line);
	while(stream>>attrib)
	{
		conditions.push_back(attrib);
		cout<<attrib<<endl;
	}
	stream.clear();

	//read the having clause predicate
	getline(file,line);
	stream.str(line);
	while(stream>>attrib)
	{
		having_predicate = attrib;
		cout<<having_predicate<<endl;
	}
	stream.clear();
	stream.clear();
	file.close();
}

void ProcessEngine::ReadQuery2(const char * fileName)
{
	//read proj number
	std::fstream file(fileName,ofstream::in);
	string proj_num_str;
	getline(file,proj_num_str);
	int proj_num = atoi(proj_num_str.c_str());
	FOR(i, proj_num)
	{
		string line;
		getline(file, line);
		proj_attrib.push_back(line);
	}

	//read the grouping attrib
	string gv_attrib_num_str;
	getline(file,gv_attrib_num_str);
	int gv_attrib_num = atoi(gv_attrib_num_str.c_str());
	FOR(i, gv_attrib_num)
	{
		string line;
		getline(file, line);
		group_attrib.push_back(line);
	}
	

	//read the grouping var number
	string group_var_num_str;
	getline(file,group_var_num_str);
	num_gourp_var  = atoi(group_var_num_str.c_str());

	//read the aggregate
	string agg_num_str;
	getline(file,agg_num_str);
	int agg_num = atoi(agg_num_str.c_str());
	FOR(i, agg_num)
	{
		string line;
		getline(file, line);
		istringstream stream(line);
		string attrib;
		int gv_num;
		string gv;
		string func;

		stream>>attrib;
		stream>>gv;
		stream>>func;
		gv_num = atoi(gv.c_str());
		aggregate_func.push_back(AggregateAttrib(attrib,gv_num,func));
	}

	//read the conditions
	string condition_num_str;
	getline(file,condition_num_str);
	int condition_num = atoi(condition_num_str.c_str());
	FOR(i, condition_num)
	{
		string line;
		getline(file, line);
		conditions.push_back(line);
	}


	//read the having clause predicate
	string having_pred;
	getline(file,having_predicate);
	file.close();
}