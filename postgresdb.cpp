#include "postgresdb.h"

PostgresDB::PostgresDB()
{
}

PostgresDB::~PostgresDB()
{
}

bool PostgresDB::connectdb(string dbname, string username, string pwd)
{
	string connectString = "dbname = '"+dbname+"' "+"user = '"+username+"' "+"password = '"+pwd+"' ";
	conn =  PQconnectdb(connectString.c_str());
	conn_rec = PQstatus(conn);
	if(conn_rec == CONNECTION_OK)
	{
		cout<<"Connection success!"<<endl;
		return true;
	}else{
		cout<<"Connection fail: "<<PQerrorMessage(conn)<<endl;
		PQfinish(conn);
		return false;
	}
}

PGresult* PostgresDB::exec_command(const char * command, ExecStatusType checkStatus)
{
	PGresult* rec = PQexec(conn,command);
	if (PQresultStatus(rec) != checkStatus)
	{
		cout<<command<<" fail !: "<<PQerrorMessage(conn)<<endl;
		PQclear(rec);
		PQfinish(conn);
		rec = NULL;
	 }
	return rec;
}

bool PostgresDB::init_result(PGresult* query_rec, result& rec_struct)
{
	rec_struct.rows = PQntuples(query_rec);
	rec_struct.columns = PQnfields(query_rec);
	rec_struct.rec = query_rec;
	for(int i = 0; i != rec_struct.columns; ++i)
	{
		string fname(PQfname(query_rec,i));
		rec_struct.fields.push_back(fname);
		rec_struct.types.push_back(PQftype(query_rec,i));
	}
	return true;
}

bool PostgresDB::start_transaction()
{
	PGresult* rec = exec_command("BEGIN",PGRES_COMMAND_OK);
	if(rec == NULL)
	{
		return false;
	}

	PQclear(rec);
	return true;
}

bool PostgresDB::end_transaction()
{
	PGresult* rec = exec_command("END",PGRES_COMMAND_OK);
	if(rec == NULL)
	{
		return false;
	}

	PQclear(rec);
	return true;
}