#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "postgresdb.h"
#include "ProcessEngine.h"

using namespace std;

ProcessEngine engine;

void Init(string filename)
{
	engine.ReadQuery2(filename.c_str());
}

int main()
{
	string query_file;
	string output_file;
	cout<<"Please enter the input query file name: ";
	cin>>query_file;
	cout<<"Please enter the ouput file name: ";
	cin>>output_file;
	cout<<"Processing....."<<endl;

	Init(query_file);

	ofstream file(output_file);
	if(file.is_open())
	{
		engine.GenIncludes(file);
		cout<<"Generate Includes files.......complete"<<endl;
		engine.GenMFStruct(file);
		cout<<"Generate MF-Structure.......complete"<<endl;
		engine.GenCheckFunc(file);
		cout<<"Generate Chech Function....complete"<<endl;
		engine.GenMainFunc(file);
		cout<<"Generate Main Function......complete"<<endl;
	}else{
		cout<<"Can't Open the File"<<endl;
	}
	file.close();

	cout<<"Press Any Key to Exit....";
	getchar();

	return 0;
}